﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void CameraController::Start()
extern void CameraController_Start_m92C8EBBE34DA314CE0C730E07400A96E968264BB (void);
// 0x00000002 System.Void CameraController::LateUpdate()
extern void CameraController_LateUpdate_m3B932A865EC4F153295CA8345D332060035F6D00 (void);
// 0x00000003 System.Void CameraController::.ctor()
extern void CameraController__ctor_m6E88C7769B5C6E7CC68A3670C07050374E05F902 (void);
// 0x00000004 System.Void MainMenu::PlayGame()
extern void MainMenu_PlayGame_mA3EE863B325BAFDC88A6354F0DE18E108CE4C4E5 (void);
// 0x00000005 System.Void MainMenu::QuitGame()
extern void MainMenu_QuitGame_mEA200DB3FF7B02D90539CD96F96EFF73F0C22485 (void);
// 0x00000006 System.Void MainMenu::.ctor()
extern void MainMenu__ctor_m63F945D965550BD614DCD2AE7F7489D4F28C5B30 (void);
// 0x00000007 System.Void MaxRec::Start()
extern void MaxRec_Start_m6A23947D45293F90D452FC9EB4910CBBFD5227B5 (void);
// 0x00000008 System.Void MaxRec::.ctor()
extern void MaxRec__ctor_m602477C7DE198165F0191CAB901C932271756394 (void);
// 0x00000009 System.Void NumberPlayer::Start()
extern void NumberPlayer_Start_m0BCC587F36EF77FF644CEA3F90D528AFD647D2D3 (void);
// 0x0000000A System.Void NumberPlayer::Update()
extern void NumberPlayer_Update_m18C390E80C2F3417B9B44EB4C9C71FF61D44D62D (void);
// 0x0000000B System.Void NumberPlayer::.ctor()
extern void NumberPlayer__ctor_m31167BF9BA3824DD3C20B039865A14D5B296E8BE (void);
// 0x0000000C System.Void PlayerController::Start()
extern void PlayerController_Start_m37E4A9612CC78B422FBFC15D257767AE6F969D78 (void);
// 0x0000000D System.Void PlayerController::Update()
extern void PlayerController_Update_mE64911CD78BC318BCCB0EDB3D9DF3BC1559C70FA (void);
// 0x0000000E System.Void PlayerController::OnMove(UnityEngine.InputSystem.InputValue)
extern void PlayerController_OnMove_mD3F002220E7E49058EEEC485614D44E6E1522705 (void);
// 0x0000000F System.Void PlayerController::setCountText()
extern void PlayerController_setCountText_mAD8905CD11E06BD509932F8BB62EB25F61C80492 (void);
// 0x00000010 System.Void PlayerController::FixedUpdate()
extern void PlayerController_FixedUpdate_m16AA033B467FF810A9C1151B616BC1D4B74F7A0E (void);
// 0x00000011 System.Void PlayerController::OnTriggerEnter(UnityEngine.Collider)
extern void PlayerController_OnTriggerEnter_m1310BAC5FA7FAE003E04BFDB6173885B41D826E7 (void);
// 0x00000012 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF66473FEFF2DFEB0325A4D9C917C27C6CE4B22F7 (void);
// 0x00000013 System.Void Rotator::Update()
extern void Rotator_Update_m5EEA78DA43EE3059DD3A2B5E0A378709E0DF21FA (void);
// 0x00000014 System.Void Rotator::.ctor()
extern void Rotator__ctor_m6FA12381051396D6275FE1B56FBD254201D5B127 (void);
// 0x00000015 System.Void Shop::Start()
extern void Shop_Start_m1107CF22ECB46A145D705B327F86EADD2E0CC30E (void);
// 0x00000016 System.Void Shop::Update()
extern void Shop_Update_m0D453940CB2C0CD48470A1A95BB66672DCFA1580 (void);
// 0x00000017 System.Void Shop::Player1()
extern void Shop_Player1_m7888C9D9186276B62CC33ADB0DF1CE1C4CCC2E23 (void);
// 0x00000018 System.Void Shop::Player2()
extern void Shop_Player2_mCDC437EBBF0815E38C76F28FCF0E12C1A5771018 (void);
// 0x00000019 System.Void Shop::Player3()
extern void Shop_Player3_m0A84AE4C8DFC1402E28519DD3C47CA7B7ACC4B18 (void);
// 0x0000001A System.Void Shop::.ctor()
extern void Shop__ctor_mF389FE7131A2AECC857ED506722A3B649DD251C1 (void);
// 0x0000001B System.Void Readme::.ctor()
extern void Readme__ctor_m23AE6143BDABB863B629ADE701E2998AB8651D4C (void);
// 0x0000001C System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_mE3D6E47455F101F2DEEBC2A58D09A97CF38E80B8 (void);
// 0x0000001D UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m73C99DB69CEB467834BBA00A62415D1CEEF0CB47 (void);
// 0x0000001E System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_mBCD24408A4A2C4053F2F98DB808BD6DE88CA998F (void);
// 0x0000001F System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_m8DE12FC1A6C31D2D60ED78F0B574CE3F864F546E (void);
// 0x00000020 System.Void Readme_Section::.ctor()
extern void Section__ctor_mE73C1D6AE5454B5A67AAB04CAA5144A5CA0B0D96 (void);
// 0x00000021 System.Void UnityTemplateProjects.SimpleCameraController_CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_m6467352ED87301E5F4A76456060A765CAB96AF3E (void);
// 0x00000022 System.Void UnityTemplateProjects.SimpleCameraController_CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_m76BCC104A48EA7F125D5A50D874A2DEEA7967247 (void);
// 0x00000023 System.Void UnityTemplateProjects.SimpleCameraController_CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController_CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_m883AAF2D3C7F5045B64CAF655FB84EF0FC98F282 (void);
// 0x00000024 System.Void UnityTemplateProjects.SimpleCameraController_CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mE3349362276789C1617C01276F7DE533BBA22623 (void);
// 0x00000025 System.Void UnityTemplateProjects.SimpleCameraController_CameraState::.ctor()
extern void CameraState__ctor_m4A83DF36C7D280050EA1B101E61B7E345C31A322 (void);
static Il2CppMethodPointer s_methodPointers[37] = 
{
	CameraController_Start_m92C8EBBE34DA314CE0C730E07400A96E968264BB,
	CameraController_LateUpdate_m3B932A865EC4F153295CA8345D332060035F6D00,
	CameraController__ctor_m6E88C7769B5C6E7CC68A3670C07050374E05F902,
	MainMenu_PlayGame_mA3EE863B325BAFDC88A6354F0DE18E108CE4C4E5,
	MainMenu_QuitGame_mEA200DB3FF7B02D90539CD96F96EFF73F0C22485,
	MainMenu__ctor_m63F945D965550BD614DCD2AE7F7489D4F28C5B30,
	MaxRec_Start_m6A23947D45293F90D452FC9EB4910CBBFD5227B5,
	MaxRec__ctor_m602477C7DE198165F0191CAB901C932271756394,
	NumberPlayer_Start_m0BCC587F36EF77FF644CEA3F90D528AFD647D2D3,
	NumberPlayer_Update_m18C390E80C2F3417B9B44EB4C9C71FF61D44D62D,
	NumberPlayer__ctor_m31167BF9BA3824DD3C20B039865A14D5B296E8BE,
	PlayerController_Start_m37E4A9612CC78B422FBFC15D257767AE6F969D78,
	PlayerController_Update_mE64911CD78BC318BCCB0EDB3D9DF3BC1559C70FA,
	PlayerController_OnMove_mD3F002220E7E49058EEEC485614D44E6E1522705,
	PlayerController_setCountText_mAD8905CD11E06BD509932F8BB62EB25F61C80492,
	PlayerController_FixedUpdate_m16AA033B467FF810A9C1151B616BC1D4B74F7A0E,
	PlayerController_OnTriggerEnter_m1310BAC5FA7FAE003E04BFDB6173885B41D826E7,
	PlayerController__ctor_mF66473FEFF2DFEB0325A4D9C917C27C6CE4B22F7,
	Rotator_Update_m5EEA78DA43EE3059DD3A2B5E0A378709E0DF21FA,
	Rotator__ctor_m6FA12381051396D6275FE1B56FBD254201D5B127,
	Shop_Start_m1107CF22ECB46A145D705B327F86EADD2E0CC30E,
	Shop_Update_m0D453940CB2C0CD48470A1A95BB66672DCFA1580,
	Shop_Player1_m7888C9D9186276B62CC33ADB0DF1CE1C4CCC2E23,
	Shop_Player2_mCDC437EBBF0815E38C76F28FCF0E12C1A5771018,
	Shop_Player3_m0A84AE4C8DFC1402E28519DD3C47CA7B7ACC4B18,
	Shop__ctor_mF389FE7131A2AECC857ED506722A3B649DD251C1,
	Readme__ctor_m23AE6143BDABB863B629ADE701E2998AB8651D4C,
	SimpleCameraController_OnEnable_mE3D6E47455F101F2DEEBC2A58D09A97CF38E80B8,
	SimpleCameraController_GetInputTranslationDirection_m73C99DB69CEB467834BBA00A62415D1CEEF0CB47,
	SimpleCameraController_Update_mBCD24408A4A2C4053F2F98DB808BD6DE88CA998F,
	SimpleCameraController__ctor_m8DE12FC1A6C31D2D60ED78F0B574CE3F864F546E,
	Section__ctor_mE73C1D6AE5454B5A67AAB04CAA5144A5CA0B0D96,
	CameraState_SetFromTransform_m6467352ED87301E5F4A76456060A765CAB96AF3E,
	CameraState_Translate_m76BCC104A48EA7F125D5A50D874A2DEEA7967247,
	CameraState_LerpTowards_m883AAF2D3C7F5045B64CAF655FB84EF0FC98F282,
	CameraState_UpdateTransform_mE3349362276789C1617C01276F7DE533BBA22623,
	CameraState__ctor_m4A83DF36C7D280050EA1B101E61B7E345C31A322,
};
static const int32_t s_InvokerIndices[37] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1134,
	23,
	23,
	23,
	26,
	1135,
	1310,
	26,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	37,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
