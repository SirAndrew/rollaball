﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MaxRec : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("Score"))
        {
            GetComponent<Text>().text = "Рекорд: " + PlayerPrefs.GetFloat("Score").ToString("F2").Replace(",", ".") + " секунд"; //если рекорд есть, вывести его
        }
        else
        {
            GetComponent<Text>().text = "Рекордов нет"; //если рекорда нет, написать что рекордов нет
        }
    }
}
