﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberPlayer : MonoBehaviour
{
    private int numberPlayer;

    public GameObject player1;
    public GameObject player2;
    public GameObject player3;
    // Start is called before the first frame update
    void Start()
    {
        numberPlayer = PlayerPrefs.GetInt("Player"); //получение цвета игрока
    }

    // Update is called once per frame
    void Update()
    {
        if (numberPlayer == 1) //если выбран 1 цвет
        {
            player1.SetActive(true);
            player2.SetActive(false);
            player3.SetActive(false);
        }

        if (numberPlayer == 2) //если выбран 2 цвет
        {
            player1.SetActive(false);
            player2.SetActive(true);
            player3.SetActive(false);
        }

        if (numberPlayer == 3) //если выбран 3 цвет
        {
            player1.SetActive(false);
            player2.SetActive(false);
            player3.SetActive(true);
        }
    }
}
