﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;

    public float timeStart = 0;
    bool timeRunning = true;
    public TextMeshProUGUI TimeText;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        TimeText.text = timeStart.ToString("F2").Replace(",", ":"); //установка времени в текстовое поле

        setCountText();
        winTextObject.SetActive(false); 
    }

    void Update()
    {
        if (timeRunning == true)
        {
            timeStart += Time.deltaTime; //счетчик времени
            TimeText.text = "Время: " + timeStart.ToString("F2").Replace(",", ".") + " сек"; //установка времени в текстовое поле
        }
    }

    private void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void setCountText()
    {
        countText.text = "Count: " + count.ToString(); //счетчик собранных монеток


        if(count >= 12) //если собраны все монетки
        {
            timeRunning = false;

            if (PlayerPrefs.GetFloat("Score") >= timeStart || !PlayerPrefs.HasKey("Score"))
            {
                PlayerPrefs.SetFloat("Score", timeStart); //если поствлен рекорд, обновление
            }
            winTextObject.SetActive(true);
            SceneManager.LoadScene("Menu"); //выход в меню
        }
    }

    private void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);

       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count++;
            setCountText();
        }
    }
}