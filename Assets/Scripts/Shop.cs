﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public GameObject tick1;
    public GameObject tick2;
    public GameObject tick3;

    private int NumberPlayers;
    // Start is called before the first frame update
    void Start()
    {
        NumberPlayers = PlayerPrefs.GetInt("Player"); //получение цвета игрока
    }

    // Update is called once per frame
    void Update()
    {
        if (NumberPlayers == 1) //если выбран 1 цвет, активация первого шарика
        {
            tick1.SetActive(true);
            tick2.SetActive(false);
            tick3.SetActive(false);
        }

        if (NumberPlayers == 2) //если выбран 2 цвет, активация второго шарика
        {
            tick1.SetActive(false);
            tick2.SetActive(true);
            tick3.SetActive(false);
        }

        if (NumberPlayers == 3) //если выбран 3 цвет, активация третьего шарика
        {
            tick1.SetActive(false);
            tick2.SetActive(false);
            tick3.SetActive(true);
        }
    }

    public void Player1()
    {
        NumberPlayers = 1;
        PlayerPrefs.SetInt("Player", NumberPlayers);
    }

    public void Player2()
    {
        NumberPlayers = 2;
        PlayerPrefs.SetInt("Player", NumberPlayers);
    }

    public void Player3()
    {
        NumberPlayers = 3;
        PlayerPrefs.SetInt("Player", NumberPlayers);
    }
}
