﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player1;
    public GameObject player2;
    public GameObject player3;

    private int numberPlayer;

    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        numberPlayer = PlayerPrefs.GetInt("Player");
        if (numberPlayer == 1)
        {
            offset = transform.position - player1.transform.position;
        }

        if (numberPlayer == 2)
        {
            offset = transform.position - player2.transform.position;
        }

        if (numberPlayer == 3)
        {
            offset = transform.position - player3.transform.position;
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (numberPlayer == 1)
        {
            transform.position = player1.transform.position + offset;
        }

        if (numberPlayer == 2)
        {
            transform.position = player2.transform.position + offset;
        }

        if (numberPlayer == 3)
        {
            transform.position = player3.transform.position + offset;
        }
    }
}